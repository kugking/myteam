/**
 * Staff.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/#!documentation/models
 */
 
module.exports = {

	attributes: {
		 

		name: {
			type: 'string'
		},
		position: {
			type: 'string'
		},
		createby: {
			type: 'string'
		},
		other: {
			type: 'string'
		},
		createdAt: {
			type: 'string'
		},
		updatedAt: {
			type: 'string'
		},
	}
};