/**
 * Staff.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/#!documentation/models
 */

module.exports = {

	attributes: {

		status: {
			type: 'string'
		},
		updateby: {
			type: 'string'
		},
		staffid: {
			type: 'string'
		},
		other: {
			type: 'string'
		},
		date: {
			type: 'string'
		},
		time: {
			type: 'string'
		},
		createby: {
			type: 'string'
		},
		createdAt: {
			type: 'string'
		},
		updatedAt: {
			type: 'string'
		},
	}
};