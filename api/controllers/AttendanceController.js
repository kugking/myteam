/**
 * AttendanceController
 *
 * @description :: Server-side logic for managing attendances
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */
var Parse = require('node-parse-api').Parse;
var options = {
  app_id: 'LMITIAEi5jV3dEpjRI3HueosLZfE29A5HElv9JMh',
  api_key: 'fovVOgIaF2NJei98VF5ZVVVLwfPcAGAf8BjMMWl3' // master_key:'...' could be used too
}
var app = new Parse(options);
var moment = require('moment');
module.exports = {



  /**
   * `AttendanceController.index()`
   */
  index: function(req, res) {
    //Tue Jan 27 2015 13:51:00 GMT+0700 (ICT) , EEE LLL d yyyy k:m:ss zzzz
    Attendance.query("SELECT attendance.id,attendance.status,attendance.staffid,attendance.updateby,attendance.other,attendance.date,attendance.time,attendance.createby,attendance.createdAt,attendance.updatedAt,staff.name,staff.position" +
      " FROM attendance LEFT JOIN staff ON staff.id=attendance.staffid GROUP BY attendance.id",
      function(error, items) {
        console.log(items);
        for (var i = 0; i < items.length; i++) {
          var time = items[i].date;
          var datetime = moment(time, "YYYY-MM-DD'T'HH:mm:ssZ").format('DD/MM/YYYY H:mm a');
          console.log(datetime);
          items[i].date = datetime;
        };
        res.view({
          attendance: items,
          user: req.cookies.user
        });
      });
  },


  /**
   * `AttendanceController.check()`
   */
  check: function(req, res) {
    return res.json({
      todo: 'check() is not implemented yet!'
    });
  },


  /**
   * `AttendanceController.update()`
   */
  update: function(req, res) {
    var date = moment(req.param("date"), 'DD/mm/YYYY H:mm a').format();
    Attendance.update(req.param("id"), {
      other: req.param("other"),
      status: req.param("status"),
      date: date,
      updateby: req.cookies.user.objectId
    }).exec(function(err, response) {
      console.log(response);
      res.redirect('/attendance/index');
    });
  },
  delete: function(req, res) {
    Attendance.destroy(req.param('id')).exec(function(error, response) {
      console.log(response);
      res.redirect('/attendance/index');
    });
  },

  /**
   * `AttendanceController.edit()`
   */
  edit: function(req, res) {
    console.log("--------------edit------------------");
    console.log("id:" + req.param("id"));
    Attendance.query("SELECT attendance.id,attendance.status,attendance.staffid,attendance.updateby,attendance.other,attendance.date,attendance.time, attendance.createby, attendance.createdAt, attendance.updatedAt, staff.name, staff.position " 
      +" FROM attendance LEFT JOIN staff ON staff.id = attendance.staffid WHERE attendance.id = " + req.param('id') + "",
      function(error, items) {
        if (error) console.log(error); res.json(error);
        
        var date = moment(items[0].date, "YYYY-MM-DD'T'HH:mm:ssZ").format('DD/MM/YYYY H:mm a');
        items[0].date = date;
        //console.log(date);
        console.log(items[0]);
        res.view({
          attendance: items[0],
        });
      });
  },
  add: function(req, res) {
    Staff.find({}).exec(function(error, items) {
      res.view({
        user: req.cookies.user,
        staffs: items
      });
    });
  },

  /**
   * `AttendanceController.create()`
   */
  create: function(req, res) {
    // time 27/07/2015 13:16 pm
    var date = moment(req.param("date"), 'DD/MM/YYYY H:mm a').format();
 
    console.log(req.param("date") + " ,, " + date+" ,,"+req.param("time"));
    Attendance.create({
      time: req.param("time"),
      staffid: req.param("selectpicker"),
      other: req.param("other"),
      status: req.param("status"),
      date: date,
      createby: req.cookies.user.objectId,
      updateby: req.cookies.user.objectId
    }).exec(function(error, item) {
      console.log(item);
      res.redirect('/attendance/index');
    });


  }
};