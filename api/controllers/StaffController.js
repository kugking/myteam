/**
 * StaffController
 *
 * @description :: Server-side logic for managing staff
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */
//Cookie:http://stackoverflow.com/questions/20368338/access-cookies-from-sails-js-controller

var Parse = require('node-parse-api').Parse;
var options = {
  app_id: 'LMITIAEi5jV3dEpjRI3HueosLZfE29A5HElv9JMh',
  api_key: 'fovVOgIaF2NJei98VF5ZVVVLwfPcAGAf8BjMMWl3' // master_key:'...' could be used too
}
var app = new Parse(options);

module.exports = {
  /**
   * `StaffController.index()`
   */
  index: function(req, res) {

    Staff.find().exec(function(error, items) {
      if (error) console.log(error);;

      console.log(items);
      res.view({
        staffs: items,
        user: req.cookies.user
      });
    });
  },


  /**
   * `StaffController.add()`
   */
  add: function(req, res) {
    console.log(req.cookies.user.username);
    res.view({
      user: req.cookies.user
    });
  },


  /**
   * `StaffController.create()`
   */
  create: function(req, res) {

    Staff.create({
      name: req.param('name'),
      position: req.param('position'),
      other: req.param('other'),
      createby: req.cookies.user.objectId

    }).exec(function(error, staff) {
      console.log(error);
      console.log("-------create----------");
      console.log(staff);
      res.redirect('/staff/index');
    });


  },


  /**
   * `StaffController.delete()`
   */
  delete: function(req, res) {
    Staff.destroy(req.param("id")).exec(function(error, response) {
      console.log(response);
      res.redirect('/staff/index');
    });
  },


  /**
   * `StaffController.remove()`
   */
  remove: function(req, res) {
    return res.json({
      todo: 'remove() is not implemented yet!'
    });
  },


  /**
   * `StaffController.show()`
   */
  show: function(req, res) {
    return res.json({
      todo: 'show() is not implemented yet!'
    });
  },


  /**
   * `StaffController.edit()`
   */
  edit: function(req, res) {
    // console.log("id:"+req.param('id'));
    Staff.findOne(req.param('id')).exec(function(error, item) {
      if (error) console.log(error);
      console.log(item);
      res.view({
        staff: item
      });
    });
  },


  /**
   * `StaffController.update()`
   */
  update: function(req, res) {
    console.log("id:" + req.param("id"));
    Staff.update(req.param("id"), {
      name: req.param('name'),
      position: req.param('position'),
      other: req.param('other')
    }).exec(function(error, item) {
      res.redirect("/staff/index");
    });
  }
};