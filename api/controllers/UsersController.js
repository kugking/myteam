/**
 * UsersController
 *
 * @description :: Server-side logic for managing users
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */
//Doc:https://github.com/Leveton/node-parse-api



// var APP_ID = 'LMITIAEi5jV3dEpjRI3HueosLZfE29A5HElv9JMh';
// var MASTER_KEY = 'leJW9vkhcnY13Hv6j6G1VZfVtoLK0ojbAUlevliZ';
// var app = new Parse(APP_ID, MASTER_KEY);

var Parse = require('node-parse-api').Parse;
var options = {
  app_id: 'LMITIAEi5jV3dEpjRI3HueosLZfE29A5HElv9JMh',
  api_key: 'fovVOgIaF2NJei98VF5ZVVVLwfPcAGAf8BjMMWl3' // master_key:'...' could be used too
}
var app = new Parse(options);

module.exports = {



  /**
   * `UsersController.index()`
   */
  index: function(req, res) {
    res.view();
  },


  /**
   * `UsersController.login()`
   */
  login: function(req, res) {
    res.view({
      layout: 'layout_login'
    });


    // app.loginUser('kavita', '12345678', function(error, response) {
    //   res.json(response);
    //   // response = {sessionToken: '', createdAt: '', ... }
    // });
  },

  verify: function(req, res) {
    app.loginUser(req.param("username"), req.param("password"), function(error, response) {
      if (error) res.json(error);
      res.cookie('user', response, {
        httpOnly: true
      });
      console.log(response);
      req.session.authenticated = true;
      res.redirect("attendance/index");
      // response = {sessionToken: '', createdAt: '', ... }
    });
  },

  /**
   * `UsersController.logout()`
   */
  logout: function(req, res) {
    return res.json({
      todo: 'logout() is not implemented yet!'
    });
  },


  /**
   * `UsersController.profile()`
   */
  profile: function(req, res) {
    app.insertUser({
      username: 'kavita',
      password: '12345678',
      email: 'kavita@365zocial.com'
    }, function(err, response) {
      res.json(response);
    });
  },


  /**
   * `UsersController.edit()`
   */
  edit: function(req, res) {
    return res.json({
      todo: 'edit() is not implemented yet!'
    });
  },


  /**
   * `UsersController.update()`
   */
  update: function(req, res) {
    return res.json({
      todo: 'update() is not implemented yet!'
    });
  },


  /**
   * `UsersController.register()`
   */
  register: function(req, res) {
    return res.json({
      todo: 'register() is not implemented yet!'
    });
  }
};